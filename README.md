### THIS PROJECT IS DEAD please refer to [_lizzy_](../../lizzy) instead

`m` is a command-line audio player in the form of a bash script with almost 0 dependency.  
It will search your music folder, let you select results from console and play them through mpv / mplayer...  
Combined with a quake-like terminal, it avails me with a perfect keyboard workflow.  
I love it and am much excited to share it: please critic. 

## Why ?
* Because moc and cmus have too long keyboard workflows.
* Because I am used to mpv bindings/behaviour, and need only minimal file selection around.
* Because my Music library is well organized (folder and files well named). But my id3-tags are a mess.

## Examples:
* `m -u` first step: quickly index your library.
* `m radioh cr` play radiohead creep. 
* `m -o -s radioh` play the first radiohead match (`-o`: oneshot). And that beeing a directory, use shuffle order (`-s`).
* `m -o -s` play random tracks of your collection.

## Features:
* customizable backend player (default is mpv)
* support playlist-files
* music repository detection (XDG_MUSIC_DIR)
* when no match found, m proposes to search with youtube-dl

## Dependencies and install:
* [mpv](https://github.com/mpv-player/mpv) (or any backend player: mplayer...)
* [sentaku](https://github.com/rcmdnk/sentaku) (on-demand download at runtime)
* [youtube-dl](https://github.com/rg3/youtube-dl) (optional)

m is a single file script: you can install it issuing for example:

`wget https://gitlab.com/Arnaudv6/m/raw/master/m ; chmod +x m`

## Discussion
on Arch forum: https://bbs.archlinux.org/viewtopic.php?id=209369


